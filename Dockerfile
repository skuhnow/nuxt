FROM node:13-alpine

# update and install dependency
RUN apk update && apk upgrade && apk add git

EXPOSE 3000

# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=3000

